import Vue from 'vue'
import Router from 'vue-router'
// import Home from '@/pages/home/Home'

Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: [{
      path: '*',
      name: 'Error',
      component: () => import('../pages/error/Error.vue')
    },{
      path: '/',
      name: 'Home',
      component: () => import('../pages/home/Home.vue')
    },{
      path: '/city',
      name: 'City',
      component: () => import('../pages/city/City.vue')
    },{
      path: '/detail/:id',
      name: 'Detail',
      component: () => import('../pages/detail/Detail.vue')
    }],

    // scrollBrhavior 路由切换时滚动到顶部
    scrollBehavior (to, from, savedPosition) {
      return {x: 0, y: 0}
    }

})
