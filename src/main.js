
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueAwesomeSwiper from 'vue-awesome-swiper'


// 引入样式
import './assets/styles/reset.css'
import './assets/styles/border.css'
import './assets/styles/iconfont.css'


//引入插件样式
import 'swiper/dist/css/swiper.css'

Vue.config.productionTip = false
Vue.use(VueAwesomeSwiper)
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
// el: '#app'是index.html挂载的